﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{

    public Animator controller;

    [Tooltip("Player Movement Speed")]
    public float deltaX = 0, deltaY = 0, deltaZ = 0;
    public enum CharacterState
    {
        IDLE = 0,
        WALKINGFORWARD = 1,
        WALKINGBACKWARDS = 2,
        JUMPING = 4,
        RUNFORWARD = 5,
        RUNBACKWARDS = 6
    }
    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;
    }
    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverState;
    PlayerState predictedState;
    // Use this for initialization
    Queue<KeyCode> pendingMoves;

    CharacterState characterAnimationState;
    void Start()
    {
        InitState();
        predictedState = serverState;
        if (isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }
    [Server]
    void InitState()
    {
        serverState = new PlayerState
        {
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f

        };
    }
    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            //Debug.Log("Pending moves: " + pendingMoves.Count);
            bool somethingPressed = false;
            KeyCode[] movementKeys = { KeyCode.A, KeyCode.S, KeyCode.D,
                KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space };
            foreach (KeyCode moveKey in movementKeys)
            {
                if (!Input.GetKey(moveKey))
                    continue;
                somethingPressed = true;
                pendingMoves.Enqueue(moveKey);
                UpdatePredictedState();
                CmdMoveOnServer(moveKey);
            }
            if (!somethingPressed)
            {
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }
        }

        SyncState();
    }
    void SyncState()
    {
        PlayerState stateToRender = isLocalPlayer ? predictedState :
            serverState;
        transform.position = new Vector3(stateToRender.posX, stateToRender.posY, stateToRender.posZ);
        transform.rotation = Quaternion.Euler(stateToRender.rotX, stateToRender.rotY, stateToRender.rotZ);
        controller.SetInteger("CharacterState", (int)stateToRender.animationState);
    }
    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaX = -0.1f;
                break;
            case KeyCode.S:
                deltaZ = -0.1f;
                break;
            case KeyCode.E:
                deltaX = 0.1f;
                break;
            case KeyCode.W:
                deltaZ = 0.1f;
                break;
            case KeyCode.A:
                deltaRotationY = -1f;
                break;
            case KeyCode.D:
                deltaRotationY = 1f;
                break;
        }
        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            posX = deltaX + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posZ,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ,
            animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY)
            
        };
    }
    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = Move(serverState, pressedKey);
    }
    void OnServerStateChanged(PlayerState newState)
    {
        serverState = newState;
        if(pendingMoves != null)
        {
            while(pendingMoves.Count > (predictedState.movementNumber -
                serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }
            UpdatePredictedState();
        }
    }
    void UpdatePredictedState()
    {
        predictedState = serverState;
        foreach(KeyCode moveKey in pendingMoves)
        {
            predictedState = Move(predictedState, moveKey);
        }
    }
    CharacterState CalcAnimation(float dX, float dY, float dZ, float dRY)
    {
        if (dX == 0 && dY == 0 && dZ == 0)
            return CharacterState.IDLE;
        if(dX != 0 || dZ != 0)
        {
            if (dX > 0 && dX < 1 || dZ > 0 && dZ < 1)
                return CharacterState.WALKINGFORWARD;
            else if (dX > 1 || dZ > 1)
            {
                return CharacterState.RUNFORWARD;
            }
            else if (dX < 0 && dX > -1 || dZ < 0 && dZ > -1)
            {
                return CharacterState.WALKINGBACKWARDS;
            }
            else
                return CharacterState.RUNBACKWARDS;
                
        }
        return CharacterState.IDLE;
    }
}
